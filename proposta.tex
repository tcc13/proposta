\documentclass[12pt,a4paper]{report}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[portuguese]{babel}
\usepackage[spaces,hyphens]{url}
\usepackage[colorlinks,allcolors=blue]{hyperref} %% optional

\begin{document}

\begin{titlepage}
  \centering
  {\huge\bfseries Estudo comparativo entre linguagens de desenvolvimento móvel
    para realidade aumentada \par}
  \vspace{2cm}
  {\Large Victor Martins João\par}
  \vspace{2cm}
  {\Large\scshape proposta de trabalho de conclusão de curso apresentado à
    disciplina\par
    MAC0499\par
    (trabalho de formatura supervisionado)}
  \vfill
  Orientador: Prof. Dr. Alfredo Goldman\par
  Coorientador: Renato Cordeiro Ferreira
  \vfill

  % Bottom of the page
  {\large São Paulo, Maio de 2021\par}
\end{titlepage}

\tableofcontents

%% ------------------------------------------------------------------------- %%

\chapter{Introdução}

O desenvolvimento móvel, por muitas vezes, visa o maior alcance de mercado
possível. Para isso os aplicativos devem ser compatíveis com os mais diversos
modelos celulares e consequentemente com seus respectivos sistemas operacionais
(em geral Android ou IOS). Ao escolher uma plataforma de desenvolvimento,
duas vertentes entram nessa discussão, as plataformas de desenvolvimento nativo
e híbrido.

As plataformas de desenvolvimento nativo são aplicações programadas
utilizando linguagem específica, que se comunicam diretamente com as APIs
(Application programming interfaces) fornecidas pelos sistemas operacionais
móveis (Android e IOS), e são compatíveis apenas com eles.
Já as plataformas de desenvolvimento híbrido propõe uma camada de abstração.
Nela é desenvolvido um código, que posteriormente é convertido em código nativo,
para interagir diretamente com as APIs de cada sistema operacional. Ou seja, com
apenas uma base de código, podem ser geradas aplicações para Android e para
IOS, evitando assim retrabalhos.

As linguagens híbridas se destacam principalmente no custo-benefício em
relação ao orçamento e cronograma diante do número de plataformas que
podem ser implementadas. Isso acontece devido à principal vantagem que é
a reutilização de código entre diferentes plataformas. No entanto, um código
específico de plataforma ainda é necessário para funcionalidades que usam
um recursos específicos~\cite{hybridPopularity}. Um exemplo no qual essa
abordagem é útil é a realidade aumentada. Uma forma de implementar tal
funcionalidade no Android é por meio da API chamada ARCore criada pelo Google
e no IOS, por meio da ARKit desenvolvida pela Apple. Ambas cumprem um mesmo
objetivo final, porém cada uma com suas especificidades para cada linguagem.

Além da realidade aumentada explorar essa fraqueza do desenvolvimento híbrido,
é uma funcionalidade que tende a ser explorada cada vez mais nos próximos anos.
Em 2021, A Grand View Research, uma empresa indo-estadunidense de consultoria
e pesquisa de mercado, cujas bases de dados são utilizadas por instituições
acadêmicas de renome no mundo todo, publicou uma pesquisa
sobre o tamanho do mercado de realidade aumentada. A pesquisa revelou que
o mercado global de realidade aumentada foi avaliado em 2020 em 17,67 bilhões
de dólares e é esperado um crescimento de 43,8\% até 2028. Esse aumento é
esperado pela crescente demanda de assistência remota e também por empresas
estarem explorando cada vez mais o potencial dessa tecnologia para oferecer
uma experiência customizada e interativa para seus clientes~\cite{ARPopularity}.

Dados os benefícios das linguagens híbridas e a crescente demanda da realidade
aumentada, este estudo propõe auxiliar desenvolvedores a escolherem com mais
propriedade qual plataforma de desenvolvimento híbrido é melhor para realidade
aumentada. Para isso será implementada uma mesma aplicação usando
duas linguagens híbridas, React Native e Flutter, e será desenvolvido um estudo
comparativo entre elas.

Neste documento será descrita a proposta de desenvolvimento da aplica-ção e do
estudo comparativo: No Capítulo 2 serão discutidos alguns conceitos relevantes
tanto para a aplicação quanto para o estudo. O Capítulo 3 é voltado ao
detalhamento da proposta do estudo e o Capítulo 4 à apresentação de um
cronograma de execução de cada etapa.

%% ------------------------------------------------------------------------- %%

\chapter{Revisão da Literatura}

\section{Conceitos gerais}
O mundo do desenvolvimento móvel é extremamente complexo, ele inclui diversos
aspectos que podem ser comparados, portanto alguns tópicos que serão discutidos
nesse estudo e serão a base do comparativo são:

\subsection{Realidade Aumentada}
Uma definição interessante proposta por Azuma~\cite{ARDefinition} é que
realidade aumentada é um sistema que possui três características
principais: Combinação de elementos reais e virtuais, interação em tempo real
e reprodução de conteúdos ou elementos em 3D. E como apresentado no capítulo
anterior, funcionalidades de realidade aumentada tendem a serem cada vez mais
comuns, o que torna interessante de ser estudada.

\subsection{Adotabilidade das linguagens}
Segundo a pesquisa de Meyerovich e Rabkin~\cite{languageAdoption}, a seleção
de uma linguagem está associada principalmente a bibliotecas, legado,
familiaridade com a linguagem e por fim efeitos históricos. Durante este
estudo serão análisados os critérios citados anteriormente para assim definir
qual linguagem possui maior de adotabilidade, ou seja, qual delas tende
a ser mais escolhida.

\subsection{Manutenibilidade}
Qualidade e estrutura de código são assuntos que logo vem a mente
quando se fala em projetos grandes e que são desenvolvidos por diversas pessoas.
Glass argumenta em seu livro~\cite{maintainabilityLifecycle} que a manutenção
consome de 40 a 80 por cento (em média, 60 por cento) do custo do software,
fazendo dela a parte mais importante do ciclo de vida de um software. Por isso,
um bom comparativo entre as plataformas é o quanto ela é manutenível a longo
prazo, seguindo suas boas práticas de desenvolvimento.

\subsection{Interface e experiência do usuário}
Os tópicos interface e experiência do usuário estão em alta quando se fala em
desenvolvimento móvel. Assim como desempenho, aplicativos com um melhor
\textit{design} e melhor experiência no uso, afetam diretamente
o quanto o usuário se sente atraído. Segundo o estudo realizado
sobre o aplicativo Kudo, um \textit{e-commerce} da Indonésia, foi observado um
incremento significativo de atração do usuário, após um processo de
\textit{redesign} da aplicação~\cite{uxAttractionIncrease}.
Por isso, nesse estudo, o processo de \textit{design} da aplicação será
cuidadoso e a experiência do usuário será um fator relevante na avaliação
final das plataformas.

\section{Plataformas a serem estudadas}
Existem diversas plataformas de desenvolvimento móvel utilizadas hoje em dia,
para desenvolvimento nativo, temos Objective-C e Swift voltado para o
desenvolvimento de aplicações IOS, e temos Java e Kotlin para desenvolvimento
Android. Além do desenvolvimento nativo, temos as opções híbridas, por exemplo,
React Native, Cordova, Xamarin e Flutter, que segundo a pesquisa do stack
overflow de 2019~\cite{stackOverflowResearch}, foram as mais usadas no ano
analisado. Ainda nessa pesquisa foi publicado sobre as plataformas mais amadas,
temidas e procuradas, e Flutter aparece como a plataforma de desenvolvimento
móvel mais amada, e React native como a mais procurada. Considerando esses
fatores, essas linguagens foram escolhidas para o desenvolvimento da aplicação
e do estudo comparativo.

%% ------------------------------------------------------------------------- %%

\chapter{Proposta}

Com o objetivo de auxiliar desenvolvedores a escolherem uma plataforma de
desenvolvimento móvel para aplicações que possuam realidade aumentada, será
desenvolvida uma aplicação móvel \textit{benchmark} utilizando as duas
plataformas citadas no Capítulo anterior. E a partir dela, será desenvolvido
um estudo comparativo analisando a implementação da aplicação e o resultado
obtido em cada linguagem.

\section{Aplicação \textit{benchmark}}
A proposta do estudo é realizar uma comparação com base nos resultados
práticos obtidos pelo autor. Portanto, nesse caso, será realizado o
desenvolvimento de uma aplicação que possua realidade aumentada. Além da
implementação desse aplicativo, fará parte desse processo a prototipação da
aplicação, o que inclui a idealização e design das telas.

\section{Estudo comparativo}
O estudo comparativo por usa vez irá analisar dois pontos
principais. O primeiro ponto será a implementação da aplicação, ou seja,
fatores relacionados a prática do desenvolvimento como aspectos da linguagem,
boas práticas, estrutura de código, problemas e soluções encontradas.
O segundo ponto será o resultado obtido com esse desenvolvimento, em outras
palavras, será analisada a aplicação de uma perspectiva do usuário, que como
discutido anteriormente, é propósito maior do desenvolvimento de uma aplicação.


%% ------------------------------------------------------------------------- %%

\chapter{Cronograma}

O projeto foi iniciado no ano passado, portanto algumas das etapas já foram
completadas, mas serão descritas a seguir para maior clareza do desenvolvimento
como um todo. O cronograma de execução do projeto pode ser dividido nas
seguintes etapas:

\begin{enumerate}
  \item \textbf{Estudo de pesquisas científicas}: Nessa etapa, que já foi
        finalizada, foi lido um artigo científico relacionado ao tema e uma
        dissertação de mestrado.

  \item \textbf{Prototipação da aplicação benchmark}: Nessa etapa, que já foi
        finalizada, foi realizada uma pesquisa de quais as funcionalidades mais
        relevantes para a realização do estudo comparativo e realidade aumentada
        possui alguns elementos importantes para esse estudo. Além disso foi
        realizado todo o processo de \emph{design} da interface dessa aplicação.

  \item \textbf{Configuração inicial do ambiente}: Nessa etapa, que já foi
        finalizada, foi inicializada a aplicação benchmark em cada uma das
        plataformas do estudo, configurado os testes automatizados iniciais,
        bem como a integração e entrega continua.

  \item \textbf{Desenvolvimento da aplicação benchmark}: Nessa etapa, que já
        foi finalizada, foi desenvolvido o protótipo de uma aplicação benchmark
        que possui uma funcionalidade de realidade aumentada.

  \item \textbf{Estudo aprofundado sobre realidade aumentada}: Nessa etapa
        será realizado um estudo mais aprofundado sobre realidade aumentada, com
        o objetivo de explicar as dificuldades observadas no desenvolvimento.

  \item \textbf{Teste de usabilidade com usuários}: Nessa etapa serão estudadas
        metodologias para teste de usabilidade com usuários. Dentre elas
        escolheremos a que melhor se aplica ao nosso caso e aplicaremos a um
        grupo de usuários. Ainda nessa fase serão analisados os resultados dos
        testes.

  \item \textbf{Monografia}: Esta etapa já foi iniciada e está ocorrendo de
        forma paralela às outras etapas.
\end{enumerate}

No calendário abaixo foram descritos o tempo gasto nas etapas já concluídas e o
tempo estimado para as restantes:

\begin{table}[h!]
  \begin{tabular}{ p{6cm}||p{3cm}|p{3cm}  }
    Etapa                                            & Início                 & Fim                    \\ [1ex]
    \hline
    Estudo de pesquisas científicas                  & 01 de Abril de 2020    & 30 de Abril de 2020    \\ [1ex]
    \hline
    Prototipação da aplicação \textit{benchmark}     & 01 de Maio de 2020     & 30 de Junho de 2020    \\ [1ex]
    \hline
    Configuração inicial do ambiente                 & 01 de Setembro de 2020 & 30 de Setembro de 2020 \\ [1ex]
    \hline
    Desenvolvimento da aplicação benchmark - Parte 1 & 01 de Outubro de 2020  & 30 de Novembro de 2020 \\ [1ex]
    \hline
    Desenvolvimento da aplicação benchmark - Parte 2 & 31 de Janeiro de 2021  & 25 de Março de 2021    \\ [1ex]
    \hline
    Estudo aprofundado sobre realidade aumentada     & 07 de Junho de 2021    & 20 de Junho de 2021    \\ [1ex]
    \hline
    Teste de usabilidade com usuários                & 19 de Julho de 2021    & 15 de Agosto de 2021   \\ [1ex]
    \hline
    Monografia                                       & 01 de Abril de 2021    & 31 de Setembro de 2021 \\ [1ex]
  \end{tabular}
  \caption{Calendário das etapas do projeto com início e fim, reais e esperados}
  \label{table:1}
\end{table}

%% ------------------------------------------------------------------------- %%

\addcontentsline{toc}{chapter}{Bibliografia}
\begin{thebibliography}{1}
  \bibitem{hybridPopularity}Meirelles P., Aguiar C.S.R., Assis F., Siqueira R., Goldman A. (2019) A Students’ Perspective of Native and Cross-Platform Approaches for Mobile Application Development. In: Misra S. et al. (eds) Computational Science and Its Applications – ICCSA 2019. ICCSA 2019. Lecture Notes in Computer Science, vol 11623. Springer, Cham. \url{https://doi.org/10.1007/978-3-030-24308-1_47}
  \bibitem{ARPopularity}Grand View Reaserch: Augmented Reality Market Size, Share \& Trends Analysis Report By Component, By Display (HMD \& Smart Glass, HUD, Handheld Devices), By Application, By Region, And Segment Forecasts, 2021 - 2028 (2021). \url{https://www.grandviewresearch.com/industry-analysis/augmented-reality-market}
  \bibitem{ARDefinition}Ronald T. Azuma; A Survey of Augmented Reality. Presence: Teleoperators and Virtual Environments 1997; 6 (4): 355–385. \url{https://doi.org/10.1162/pres.1997.6.4.355}
  \bibitem{languageAdoption}Meyerovich, Leo \& Rabkin, Ariel. (2013). Empirical Analysis of Programming Language Adoption. ACM SIGPLAN Notices. 48. 1-18. \url{http://doi.org/10.1145/2509136.2509515}.
  % \bibitem{learningCurveDefinition} \url{https://www.valamis.com/hub/learning-curve}
  % \bibitem{mobileProgrammingLanguages} \url{https://usemobile.com.br/framework-desenvolvimento-aplicativos-2019/}
  \bibitem{maintainabilityLifecycle} Glass, Robert L. Facts and Fallacies of Software Engineering. Boston, United States: Addison-Wesley, 2002.
  \bibitem{uxAttractionIncrease} Muslim, Erlinda \& Moch, Boy \& Wilgert, Yosua \& Utami, Fristya \& Indriyani, Dea. (2019). User interface redesign of e-commerce platform mobile application (Kudo) through user experience evaluation to increase user attraction. IOP Conference Series: Materials Science and Engineering. 508. 012113. \url{https://doi.org/10.1088/1757-899X/508/1/012113}
  \bibitem{stackOverflowResearch}StackOverflow: Developer Survey Results 2019 (2019), \url{https://insights.stackoverflow.com/survey/2019}
\end{thebibliography}

\end{document}